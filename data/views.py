from django.shortcuts import render, redirect
from django.contrib.auth.models import User, auth
from django.core.paginator import Paginator
from django.contrib import messages
from .models import Data,Task, Notebook, Sign_Up
from django.core.mail import send_mail   
from django.conf import settings
from django.utils import timezone
import requests
import json



# Create your views here.
def index(request):
    return render(request,'data/index.html')

def login(request):
    if request.method =='POST':
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=username,password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('home')
        else:
            messages.info(request, 'Invalid username or password')
            return redirect('log')
    else:   
        return render(request, 'data/login.html') 

def registration(request):
    if request.method == "POST":
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        password2 = request.POST['re_pass']

        if(password != password2):
            messages.info(request, 'password 1 does not match password 2 ')
            return redirect('reg2')

        elif User.objects.filter(username=username).exists():
            messages.info(request, 'Username already exists')
            return redirect('reg2')

        else:
            u = User.objects.create_user(username=username, email=email, password=password)
            u.save();

            return redirect('log')
    else:
        return render(request,'data/registration.html')

def home(request):
    return render(request, 'data/home.html')

def logout(request):
    auth.logout(request)
    return redirect('/')

def log(request):
    return render(request, 'data/login.html')

def reg2(request):
    return render(request, 'data/reg2.html')

#view for the datasets
def dataset(request):
    now = timezone.now()
    D = Data.objects.all().order_by('-year')
    paginator = Paginator(D, 7)
    page_number = request.GET.get('page')
    D = paginator.get_page(page_number)
    context = {'D':D}
    return render(request, 'data/dataset.html', context)


#view for the details page
def details(request, id):
    file = Data.objects.get(id=id)
    ta = Task.objects.get(id=id)
    context = {'file':file, 'ta':ta}
    return render(request, 'data/detail.html', context)


#view for the courses
def course(request):
    return render(request, 'data/courses.html')


#view for the uploads
def upload(request):
    return render(request, 'data/upload.html')

#view for the contacts
def contact(request):
    return render(request, 'data/contact.html')


#view for the task
def task(request, id):
    file = Data.objects.get(id=id)
    ta = Task.objects.get(id=id)
    context = {'file':file, 'ta':ta}
    return render(request, 'data/task.html', context)

#view for the meta
def meta(request, id):
    file = Data.objects.get(id=id)
    ta = Task.objects.get(id=id)
    context = {'file':file, 'ta':ta}
    return render(request, 'data/metadata.html', context)


#views for the activity
def activity(request, id):
    file = Data.objects.get(id=id)
    ta = Task.objects.get(id=id)
    context = {'file':file, 'ta':ta}
    return render(request, 'data/activity.html', context)


#view for the task main work
def work(request):
    file = Data.objects.get()
    ta = Task.objects.get()
    context = {'file':file, 'ta':ta}
    return render(request, 'data/activity.html', context)



#views for processing the emails
def mail(request):
    if request.method == "POST":
        subject = request.POST['name']
        fro = request.POST['email']
        msg = request.POST['msg']
        to = settings.EMAIL_HOST_USER
        res = send_mail([subject, fro], msg, fro, [to])
        if res == 1:
            return redirect('ques')
        else:
            messages.info(request, "Message Could not be sent")
            return redirect('contact')

    else:
        messages.info(request, "Message Could not be sent")
        return redirect('contact')


# mail subscription with mailchimp
MAILCHIMP_API_KEY = settings.MAILCHIMP_API_KEY
MAILCHIMP_DATA_CENTER = settings.MAILCHIMP_DATA_CENTER
MAILCHIMP_EMAIL_LIST_ID = settings.MAILCHIMP_EMAIL_LIST_ID

api_url = f'https://{MAILCHIMP_DATA_CENTER}.api.mailchimp.com/3.0'
members_endpoint = f'{api_url}/lists/{MAILCHIMP_EMAIL_LIST_ID}/members'


def subscribe(email):
    data = {
    "email_address" :email,
    "status" : "subscribed"
    }

    r = requests.post(
        members_endpoint,
        auth = (" ", MAILCHIMP_API_KEY),
        data = json.dumps(data)
    )

    return r.status_code, r.json()

def list(request):
    if request.method == "POST":
        email = request.POST['email']

        if Sign_Up.objects.filter(email=email).exists():
            messages.info(request, "email already subscribed")
            return redirect('contact')

        else:
            mes = Sign_Up(email=email)
            mes.save()
            return redirect('success')

    else:
        return redirect('contact')




def success(request):
    return render(request, 'data/mail_success.html')


def forgot(request):
    return render(request, 'registration/password_reset_form.html')

def ques(request):
    return render(request, 'data/success_contact.html')