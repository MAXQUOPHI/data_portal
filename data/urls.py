from django.conf.urls import url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from .import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    url(r'^$', views.index, name='index'),    
    url('login', views.login, name='login'),
    url('logout', views.logout, name='logout'),
    url('registration', views.registration, name='registration'),
    url('log', views.log, name='log'),
    url('reg2', views.reg2, name='reg2'),
    url('home', views.home, name='home'),
    url('mail', views.mail, name='mail'),
    url('dataset', views.dataset, name='dataset'),
    url('course', views.course, name='course'),
    url('upload', views.upload, name='upload'),
    url('contact', views.contact, name='contact'),
    url('success', views.success, name='success'),
    url('ques', views.ques, name='ques'),
    url('forgot', views.forgot, name='forgot'),
    url('list', views.list, name='list'),
    url('^task/(?P<id>\d+)/$', views.task, name='task'),
    url('^meta/(?P<id>\d+)/$', views.meta, name='meta'),
    url('^activity/(?P<id>\d+)/$', views.activity, name='activity'),
    url('work', views.work, name='work'),
    url('^details/(?P<id>\d+)/$', views.details, name ='details'),
   
    

]


