from django.db import models

# Create your models here.
class Data(models.Model):
	VISIBILITY = (
		('Public', 'Public'),
		('Private', 'Private')
		)
	name = models.CharField(max_length = 550)
	license = models.TextField(max_length = 300, blank= True)
	visibility = models.CharField(max_length = 7, choices = VISIBILITY, blank=True)
	dat = models.FileField(upload_to = 'file')
	readme = models.FileField(blank=True)
	image = models.ImageField(default = 'id.jpg', blank=True)
	author =  models.CharField(max_length = 250 , blank = True)
	tags = models.TextField(max_length = 500, blank = True )
	maintainers = models.TextField(max_length = 300, blank = True)
	#summary = models.TextField(max_length = 300, blank=True)
	year = models.DateField()
	acknowledge = models.TextField(max_length = 500, blank = True)
	usability = models.IntegerField(default= 5.5)
	links = models.CharField(max_length = 1000, blank = True)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name_plural = "Data"



class Task(models.Model):
	name = models.CharField(max_length = 200, blank=True)
	summary = models.TextField(max_length = 1000, blank = True)
	detail = models. TextField(max_length = 9000, blank = True)
	expected = models.TextField(max_length = 2000, blank = True)
	submission = models.TextField(max_length = 2000, blank = True)
	evaluation = models.TextField(max_length = 2000, blank = True)
	timeline = models.TextField(max_length = 500, blank = True)
	prizes = models.TextField(max_length = 500, blank = True)
	task_name = models.ForeignKey(Data, on_delete = models.CASCADE)


	def __str__(self):
		return self.name


	class Meta:
		verbose_name_plural = "Task"


class Notebook(models.Model):
	notebook_name = models.CharField(max_length=500, blank=True, default='')
	notebook = models.FileField(blank=False)
	data = models.ForeignKey(Data, on_delete=models.CASCADE)


	def __str__(self):
		return self.notebook_name

	class Meta:
		verbose_name_plural = "Notebook"



class Sign_Up(models.Model):
	email =models.EmailField()
	timestamp = models.DateTimeField(auto_now_add = True)

	def __str__(self):
		return self.email

	class Meta:
		verbose_name_plural = "Sign_Up"


		



#q = models.ForeignKey(head, on_delete=models.CASCADE)

