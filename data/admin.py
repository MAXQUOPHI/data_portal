from django.contrib import admin
from .models import Data,Task, Notebook, Sign_Up

# Register your models here.
admin.site.site_header = 'Data_portal'
admin.site.site_title = 'Data_Portal'
admin.site.index_title = 'Data_portal'
admin.site.register(Data)
admin.site.register(Task)
admin.site.register(Notebook)
admin.site.register(Sign_Up)